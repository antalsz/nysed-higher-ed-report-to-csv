# Convert a NYSED higher education report to CSV

**To use:** Navigate to a NYSED higher education report in your browser, open
the JavaScript console (on macOS, that's <kbd>⌘⌥C</kbd> in Safari or
<kbd>⌘⌥J</kbd> in Chrome), paste the entire contents of
[`nysed-higher-ed-report-to-csv.js`](nysed-higher-ed-report-to-csv.js) there,
and hit return.  The browser will then download – or, depending on your security
settings, offer to download – that NYSED higher education report as a CSV.
