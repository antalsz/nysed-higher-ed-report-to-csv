'use strict';

/******************************************************************************
 * TO USE: Navigate to a NYSED higher education report in your browser, open  *
 * the JavaScript console (on macOS, that's ⌘⌥C in Safari or ⌘⌥J in Chrome),  *
 * paste the entire contents of this file there, and hit return.  The browser *
 * will then download – or, depending on your security settings, offer to     *
 * download – that NYSED higher education report as a CSV.                    *
 ******************************************************************************/

function Enum(...names) {
  return Object.freeze(Object.fromEntries(names.map(name => [name, Symbol(name)])));
}

function arrayEqual(arr1, arr2, eq = (x, y) => x === y) {
  return arr1.length === arr2.length && arr1.every((x, i) => eq(x, arr2[i]));
}

function histogramMap(f, arr) {
  const map = new Map();
  arr.forEach((x, i) => {
    const [k, v] = f(x, i);
    let vs = map.get(k);
    if (!vs)
      map.set(k, vs = []);
    vs.push(v);
  });
  return map;
}

function histogram(f, arr) {
  return histogramMap((x, i) => [f(x, i), x], arr);
}

function capitalizeFirst(str) {
  str = String(str);
  return str && (str[0].toUpperCase() + str.substring(1));
}

class ParseError extends Error {
  static {
    this.prototype.name = 'ParseError';
  }

  #text;
  #value;
  #index;

  get text() { return this.#text; }
  get value() { return this.#value; }
  get index() { return this.#index; }
  
  set text(text) {
    this.#text = text;
    this.#updateMessage();
  }

  set value(value) {
    this.#value = value;
    this.#updateMessage();
  }

  set index(index) {
    this.#index = index;
    this.#updateMessage();
  }

  #updateMessage() {
    let message = '';
    if (this.#index !== undefined)
      message = `In record ${this.#index}: `;
    message += this.#text;
    if (this.#value !== undefined)
      message += `; got: ${JSON.stringify(this.#value)}`;
    
    this.message = message;
  }
  
  constructor(text, value = undefined, index = undefined) {
    super(text);
    this.#text = text;
    this.#value = value;
    this.#index = index;
    this.#updateMessage();
  }
}
Object.freeze(ParseError);

class LineIterator {
  #lines       = [];
  #currentLine = 0;

  constructor(lines) {
    this.#lines = Array.from(lines, line => {
      if (line === undefined || line === null)
        throw new TypeError('Lines must be defined and nonnull');
      return line;
    });
  }

  next() {
    return this.#currentLine >= this.#lines.length
             ? null
             : this.#lines[this.#currentLine++];
  }

  static #Consume = Enum('Stop', 'PutBack', 'Continue');
  static get Consume() { return this.#Consume; }

  consume(transition) {
    while (this.#currentLine < this.#lines.length) {
      const line = this.#lines[this.#currentLine];
      switch (transition(line)) {
        case LineIterator.Consume.PutBack:
          return null;
        case LineIterator.Consume.Stop:
          ++this.#currentLine;
          return line;
        case LineIterator.Consume.Continue:
          ++this.#currentLine;
          break;
        default:
          throw new TypeError(
            'Invalid return value from "consume" transition function');
      }
    }

    throw new Error('"consume" ran out of lines without finishing');
  }
  
  reset() {
    this.#currentLine = 0;
  }
}
Object.freeze(LineIterator);

// Convert a line like '  --- ------ --- ' to start-end pairs for each
// hyphen-spanned column.
function headerSeparatorToColumnSpans(separator) {
  const columns = [];

  for (const {indices: [[start, end]]} of separator.matchAll(/(?<!-)-+(?!-)/ugd))
    columns.push(Object.freeze({start, end}));

  return Object.freeze(columns);
}

function invertColumns(columns) {
  // Assumes columns are nonoverlapping

  const inverted = [];
  
  let prev = 0;

  for (const {start, end} of columns.toSorted(
                               (a, b) => a.start - b.start || a.end - b.end))
  {
    if (start > prev)
      inverted.push(Object.freeze({start: prev, end: start}));
    prev = end;
  }

  return {
    inverted: Object.freeze(inverted),
    end: prev
  };
}

function getColumns(columns, lines) {
  const parsedLines =
    lines.map(line =>
      columns.map(({start, end}) => line.substring(start, end).replace(/ *$/u, ''))
    );
  
  const result = [];
  for (let i = 0; i < columns.length; ++i) {
    result.push(parsedLines.map(line => line[i]));
  }

  return result;
}

function readPreamble(iterator) {
  let umbrellas = [];
  let headerLines = [];

  const separator = iterator.consume(line => {
    if (line.match(/^[ \-]*$/u) && line.includes('-')) {
      return LineIterator.Consume.Stop;
    } else {
      if (line.match(/^ *$/u)) {
        umbrellas = [];
        headerLines = [];
      } else {
        line = line.replaceAll(
          /(?<!-)-{2,} (.+?) -{2,}(?!-)/ug,
          (match, name, start) => {
            const index = headerLines.length;
            const length = match.length;
            const end = start + length + 1;
            umbrellas.push({index, name, start, end});
            return ' '.repeat(length);
          }
        );
        headerLines.push(line);
      }
      return LineIterator.Consume.Continue;
    }
  });

  const columns = headerSeparatorToColumnSpans(separator);
  const headings = getColumns(columns, headerLines);

  const result = [];
  for (let i = 0; i < columns.length; ++i) {
    const {start, end} = columns[i];
    const headingRows = headings[i];

    const umbrella = umbrellas.find(umbrella =>
      headingRows.findIndex(s => s !== '') > umbrella.index &&
      umbrella.start <= start && end <= umbrella.end
    );

    let heading = Object.freeze(
      headingRows.filter(s => s !== '')
                 .map(s => s.replaceAll(/ +/ug, ' '))
    );
    if (umbrella) {
      heading = Object.freeze({
        category: umbrella.name,
        heading:  Object.freeze(heading)
      });
    }
    
    result.push(Object.freeze({heading, start, end}));
  }

  return result;
}

class Column {
  #subfields;
  #name;
  
  static #Count = Enum('Required', 'Optional', 'Some', 'Many');
  static get Count() { return this.#Count; }
  
  static #subfield([name, count, parse]) {
    return Object.freeze({name, count, parse});
  }

  constructor(...args) {
    if (args.length === 3 && typeof args[0] === 'string') {
      const [name, _count, _parse] = args;
      this.#subfields = Object.freeze([Column.#subfield(args)]);
      this.#name = name;
    } else {
      this.#subfields = Object.freeze(args.map(arg => Column.#subfield(arg)));
      this.#name = Object.freeze(this.#subfields.map(sf => sf.name));
    }
    Object.freeze(this);
  }

  #nameForError() {
    return typeof this.#name === 'string'
             ? this.#name
             : Column.#headingSingleName(this.#name);
  }

  static #headingSingleName(heading) {
    if (Array.isArray(heading)) {
      return heading.join(heading.every(s => s.length === 1) ? '' : ' ');
    } else {
      return `${heading.category}: ${Column.#headingSingleName(heading.heading)}`;
    }
  }
  
  isForHeading(heading) {
    return typeof this.#name === 'string'
             ? Column.#headingSingleName(heading) === this.#name
             : Array.isArray(heading) && arrayEqual(heading, this.#name);
  }

  static #parseSubfield({name, count, parse}, rows, i) {
    const one = missing => ({
      result: i >= rows.length || rows[i] === '' ? missing() : parse(rows[i]),
      index:  i + 1
    });

    const multiple = validate => {
      const resultRows = rows.slice(i);
      validate(resultRows);
      return {
        result: resultRows.map(row => parse(row)),
        index:  rows.length
      };
    };
    
    switch (count) {
      case Column.Count.Required:
        return one(() => { throw new ParseError(`${name} was missing`); });
      case Column.Count.Optional:
        return one(() => null);
      case Column.Count.Some:
        return multiple(resultRows => {
          if (resultRows.every(s => s === ''))
            throw new ParseError(`${name} was missing`);
        });
      case Column.Count.Many:
        return multiple(_ => {});
      default:
        throw new TypeError(
          `Subfield ${name} has an invalid count (${count?.toString() ?? count})`);
    }
  }

  parse(rows) {
    let index = 0;

    const parsed = this.#subfields.map(subfield => {
      const parsed = Column.#parseSubfield(subfield, rows, index);
      index = parsed.index;
      return [subfield.name, parsed.result];
    });

    if (rows.slice(index).some(row => row !== ''))
      throw new ParseError(`Column ${this.#nameForError()} has extra rows`, rows);

    return parsed;
  }
}
Object.freeze(Column);

class Schema {
  #columns;
  #noncolumnar;
  #whitespace;
  #startChar;
  #endChar;

  static isBlank(s) {
    return /^ *$/u.test(s);
  }

  static #throwNoncolumnar(_acc, line) {
    throw new ParseError('Line had non-columnar data', line);
  }
  
  constructor(headingSpans, columns, noncolumnar = undefined) {
    this.#columns = [];
    for (const {heading, start, end} of headingSpans) {
      const column = columns.find(col => col.isForHeading(heading));

      if (!column)
        throw new ParseError('Unknown column', heading);
      
      this.#columns.push(Object.freeze({column, start, end}));
    }
    Object.freeze(this.#columns);

    this.#noncolumnar = noncolumnar ?? Schema.#throwNoncolumnar;
    this.#startChar = this.#columns[0]?.start ?? 0;
    ({ inverted: this.#whitespace, end: this.#endChar } = invertColumns(this.#columns));
    
    Object.freeze(this);
  }

  lineStartsWith(line, start) {
    return (
      start.length === 1
        ? line[this.#startChar] === start
        : line.substring(this.#startChar).startsWith(start)
    );
  }

  stripLineStart(line, start) {
    if (this.lineStartsWith(line, start)) {
      const length = start.length;
      return line.substring(0, this.#startChar) +
             ' '.repeat(length) +
             line.substring(this.#startChar+length);
    } else {
      return null;
    }
  }

  allInsideColumns(line) {
    return (
      Schema.isBlank(line.substring(this.#endChar)) &&
      this.#whitespace
          .map(({start, end}) => line.substring(start, end))
          .every(Schema.isBlank)
    );
  }

  parseEntry(lines) {
    const checkedLines = histogram(line => this.allInsideColumns(line), lines);
    const columnarLines = checkedLines.get(true) ?? [];
    const noncolumnarLines = checkedLines.get(false) ?? [];
    
    const columns = new Map(
      getColumns(this.#columns, columnarLines).flatMap((column, i) =>
        this.#columns[i].column.parse(column)
      )
    );

    return noncolumnarLines.reduce(
      (acc, line) => this.#noncolumnar(acc, line),
      columns
    );
  }
}
Object.freeze(Schema);

function readEntries(iterator, schema) {
  const entries = [];

  for (let line, entry; (line = iterator.next()) !== null;) {
    if (Schema.isBlank(line))
      continue;

    const foundPlaceHolder = schema.stripLineStart(line, 'PLACE HOLDER');
    if (foundPlaceHolder) {
      if (!entry)
        throw new ParseError('An entry was placeholdered before any were started');
      entry.placeholder = true;
      line = foundPlaceHolder;
    }

    const lsw = schema.lineStartsWith(line, ' ');
    if (!lsw) {
      entry = { placeholder: false, lines: [] };
      entries.push(entry);
    }

    if (!entry)
      throw new ParseError('An entry was continued before any were started');

    entry.lines.push(line);
  }

  return Object.freeze(entries.map(({placeholder, lines}, i) => {
    try {
      const result = schema.parseEntry(lines);
      result.set('PLACE HOLDER', placeholder);
      return Object.freeze(new Map(result));
    } catch (exn) {
      if (exn instanceof ParseError)
        exn.index = i;
      throw exn;
    }
  }));
}

// Generally useful
const monthYearRx = /(?:(?:0[1-9]|1[0-2])\/(?:19|20)[0-9]{2})/u;

/** An `ItemType` contains four fields, which are provided to the constructor in
    an appopriate object:
    
    ```
        name:     NonemptyString,
        validate: any => Bool throws TypeError
        csv:      { x : any | validate(any) } => String | Array[String]
        header:   null | String | Array[String]
    
    ```
    
    These fields have the following properties:
    
    * `name` says what this item is, for use in error messages

    * `validate` is a predicate that determines whether a value has the right
       type; it may also call the `checkType` method on other `ItemType`s
       internally to throw its own `TypeError`s.  It is also passed a string
       saying what this value is, for passing to `checkType`; this can be
       ignored if that's not necessary.

    * `csv` serializes a value into a CSV field (a string) or row (an array of
      strings); the input value must be of the right type, i.e. one for for
      which `type` returns `true`.

    * `header` is the header for this CSV column (a string) or columns (an array
      of strings); it must be the same type and, if an array, length as the
      result of `csv`.  It may also be omitted (null), but only if `csv` returns
      a string.
    
    The `name` and `header` fields can be accessed directly; the `validate` and
    `csv` fields can only be accessed indirectly, via the methods `checkType`
    and `toCSV`, respectively.
 */
class ItemType {
  #name;     /* NonemptyString */
  #validate; /* (any, String) => Bool throws TypeError */
  #csv;      /* { x : any | validate(any) } => String | Array[String] */
  #header;   /* null | String | Array[String] */
  
  constructor({name, validate, csv, header, ...rest}) {
    const extraArgs = Object.getOwnPropertyNames(rest);
    if (extraArgs.length !== 0) {
      throw new TypeError(
        'ItemType constructor got an argument with extra fields' +
        ` (${extraArgs.join(', ')})`
      );
    }

    const checkArg = (ok, argName, what) => {
      if (!ok)
        throw new TypeError(`An ItemType's ${argName} must be ${what}`);
    };

    const checkArgFunction = (thing, what) =>
      checkArg(typeof thing === 'function', what, 'a function');

    checkArg(typeof name === 'string' && name !== '', 'name', 'a nonempty string');
    checkArgFunction(validate, 'type predicate');
    checkArgFunction(csv, 'CSV serializer');
    checkArg(
      header === null ||
      typeof header === 'string' ||
      (Array.isArray(header) && header.every(x => typeof x === 'string')),
      'CSV header',
      'null, a string, or an array of strings'
    );

    this.#name     = name;
    this.#validate = validate;
    this.#csv      = csv;
    this.#header   = Array.isArray(header) ? Object.freeze([...header]) : header;
    Object.freeze(this);
  }

  get name() {
    return this.#name;
  }

  get header() {
    return this.#header;
  }

  withHeader(header) {
    if (this.header !== null)
      throw new TypeError('Cannot call withHeader on an ItemType with a nonnull header');

    return this.withNewHeader(header);
  }

  withNewHeader(header) {
    return new ItemType({
      name:     this.#name,
      validate: this.#validate,
      csv:      this.#csv,
      header
    });
  }

  #invalidTypeMessage(what) {
    return `${what} is not ${this.name}`;
  }

  checkType(x, what) {
    let wellTyped;
    try {
      wellTyped = this.#validate(x, what);
    } catch (exn) {
      if (exn instanceof TypeError)
        exn.message += `\n  so ${this.#invalidTypeMessage(what)}`;
      throw exn;
    }

    if (!wellTyped)
      throw new TypeError(`${this.#invalidTypeMessage(what)} (got: ${x})`);

    return true; // Makes certain patterns easier
  }

  toCSV(x, checkType = false) {
    if (checkType)
      this.checkType(x, 'CSV serialization target');
    // Assume that `this.checkType(x)` would have succeeded regardless
    
    const csv = this.#csv(x);

    if (typeof csv === 'string') {
      if (Array.isArray(this.header)) {
        throw new TypeError(
          "An ItemType's CSV serializer returned one field when multiple " +
          "fields were expected"
        );
      }
    } else if (Array.isArray(csv)) {
      if (!csv.every(x => typeof x === 'string')) {
        throw new TypeError(
          "An ItemType's CSV serializer returned an array containing a " +
          "non-string value"
        );
      }
      
      if (!Array.isArray(this.header)) {
        throw new TypeError(
          "An ItemType's CSV serializer returned multiple fields when only " +
          "one field was expected"
        );
      }

      if (csv.length !== this.header.length) {
        throw new TypeError(
          "An ItemType's CSV serializer returned the wrong number of items: " +
          `got ${csv.length}, expected ${this.header.length}`
        );
      }
    } else {
      throw new TypeError(
        "An ItemType's CSV serializer returned a non-string, non-array value"
      );
    }

    return csv;
  }

  /****************************************************************************/

  // Type algebra operators; these require access to the private members

  static defaultUnionExns(exn1, exn2) {
    if (exn1 === null) throw exn2;
    if (exn2 === null) throw exn1;
    return false;
  }

  static defaultIntersectionCSVs(mkCSV1, mkCSV2) {
    const csv1 = mkCSV1();
    const csv2 = mkCSV2();
    
    if (!(Array.isArray(csv1) && Array.isArray(csv2)
            ? arrayEqual(csv1, csv2)
            : csv1 === csv2))
    {
      throw new TypeError(
        'Unequal CSVs when taking the intersection of two different ItemTypes'
      );
    }

    return csv1;
  }

  static defaultUnionNames;
  static defaultUnionHeaders;
  static defaultIntersectionNames;
  static defaultIntersectionHeaders;
  
  static {
    function defaultMergers(verb, conjunction) {
      const headerConjunction = capitalizeFirst(conjunction);
      const stringHeader = (h1, h2) => `${h1} ${headerConjunction} ${h2}`;
   
      return [
        (name1, name2) => `${name1} ${conjunction} ${name2}`,
   
        (header1, header2) => {
          if (Array.isArray(header1) && Array.isArray(header2) &&
              header1.length === header2.length)
          {
            return header1.map((h1, i) => stringHeader(h1, header2[i]));
          } else if (header1 === null && header2 === null) {
            return null;
          } else if (header1 === null && typeof header2 === 'string') {
            return header2;
          } else if (typeof header1 === 'string' && header2 === null) {
            return header1;
          } else if (typeof header1 === 'string' && typeof header2 === 'string') {
            return stringHeader(header1, header2);
          } else {
            throw new TypeError(
              `Cannot ${verb} ItemTypes expecting different numbers of CSV columns`
            );
          }
        }
      ];
    }

    [ItemType.defaultUnionNames, ItemType.defaultUnionHeaders] =
      defaultMergers('union', 'or');

    [ItemType.defaultIntersectionNames, ItemType.defaultIntersectionHeaders] =
      defaultMergers('intersection', 'and');
  }
  
  static union(typ1, typ2,
               unionExns = ItemType.defaultUnionExns,
               unionNames = ItemType.defaultUnionNames,
               unionHeaders = ItemType.defaultUnionHeaders)
  {
    const check = (typ, x, what) => {
      try {
        return typ.#validate(x, what) || null;
      } catch (exn) {
        if (exn instanceof TypeError)
          return exn;
        else
          throw exn;
      }
    };
    
    return new ItemType({
      name: unionNames(typ1.name, typ2.name),
      
      validate: (x, what) => {
        const r1 = check(typ1, x, what);
        if (r1 === true)
          return true;
        const r2 = check(typ2, x, what);
        if (r2 === true)
          return true;

        if (!r1 && !r2)
          return false;

        return Boolean(unionExns(r1, r2));
      },

      csv: x =>
        check(typ1, x, 'CSV serialization target') === true ? typ1.#csv(x) : typ2.#csv(x),

      header: unionHeaders(typ1.header, typ2.header)
    });
  }

  // Will preferentially throw an exception for `typ1.checkType`
  static intersection(typ1, typ2,
                      intersectionCSVs = ItemType.defaultIntersectionCSVs,
                      intersectionNames = ItemType.defaultIntersectionNames,
                      intersectionHeaders = ItemType.defaultIntersectionHeaders)
  {
    return new ItemType({
      name: intersectionNames(typ1.name, typ2.name),
      validate: (x, what) => typ1.checkType(x, what) && typ2.checkType(x, what),
      csv: x => intersectionCSVs(() => typ1.#csv(x), () => typ2.#csv(x)),
      header: intersectionHeaders(typ1.header, typ2.header)
    });
  }
}
Object.freeze(ItemType);

class BasicTypes {
  constructor() {
    throw new Error('BasicTypes is a namespace of static methods');
  }
  
  static null_ = new ItemType({
    name: 'null',
    validate: x => x === null,
    csv: _ => '',
    header: null
  });

  static bool = new ItemType({
    name: 'a boolean',
    validate: x => typeof x === 'boolean',
    csv: b => String(b),
    header: null
  });

  static string = new ItemType({
    name: 'a string',
    validate: x => typeof x === 'string',
    csv: s => s,
    header: null
  });

  static nonemptyString = new ItemType({
    name: 'a nonempty string',
    validate: x => typeof x === 'string' && x !== '',
    csv: s => s,
    header: null
  });

  static literal(...literals) {
    const literalSet = new Set(literals);
    
    if (!(literals.every(lit => typeof lit === 'string') &&
          literals.length === literalSet.size))
    {
      throw new TypeError('BasicTypes.literals expects an array of unique strings');
    }

    const names = literals.map(lit => JSON.stringify(lit));

    const name =
      literals.length === 1
        ? names[0]
        : names.slice(0, -1).join(', ') + ' or ' + names[names.length-1];
    
    return new ItemType({
      name,
      validate: x => typeof x === 'string' && literalSet.has(x),
      csv: s => s,
      header: null
    });
  }

  static regexp(name, srcRx, header = undefined)  {
    if (!(srcRx instanceof RegExp)) {
      throw new TypeError(
        'BasicTypes.regexp expects a regular expression as its second argument'
      );
    }
    
    const rx = new RegExp(String.raw`^(?:${srcRx.source})$`, 'u');
    return new ItemType({
      name,
      validate: x => typeof x === 'string' && rx.test(x),
      csv: s => s,
      header: header === undefined ? capitalizeFirst(name) : header
    });
  }
  
  // Ignores most of the complexity from its argument during the union process;
  // for instance, it's probably best if `simple.checkType` can't throw, and
  // `simple.header` is ignored (so it should probably be `null`).
  static optionalWith(simple) {
    return complex => ItemType.union(
      simple, complex,
      (_exnS, exnC) => exnC,
      (nameS, nameC) => ItemType.defaultUnionNames(nameC, nameS),
      (_headerS, headerC) => headerC
    );
  }

  static optional = BasicTypes.optionalWith(BasicTypes.null_);

  static frozenObject = new ItemType({
    name: 'frozen',
    validate: x => Object.isFrozen(x),
    csv: x => String(x),
    header: null
  });

  static #articulation(name) {
    return /^(a|an) /uid.exec(name)?.indices[0][1];
  }
  
  static #dearticulate(name) {
    const start = BasicTypes.#articulation(name);
    return start === undefined ? name : name.substring(start);

  }

  static #rearticulate(article, adjective, name) {
    const start = BasicTypes.#articulation(name);
    if (start === undefined) {
      return `${adjective} ${name}`;
    } else {
      const casedArticle = name[0] === 'A' ? capitalizeFirst(article) : article;
      return `${casedArticle} ${adjective} ${name.substring(start)}`;
    }
  }

  static frozen(typ) {
    // Frozenness is JavaScript-only, it doesn't affect the CSV
    return ItemType.intersection(
      typ, BasicTypes.frozenObject,
      (csv, _csvFO) => csv(),
      (name, _nameFO) => BasicTypes.#rearticulate('a', 'frozen', name),
      (header, _headerFO) => header
    );
  }

  static array(typ, csv = undefined, header = undefined) {
    const wholeCSV = csv !== undefined ? csv : arr => arr.join(', ');
    const arrayHeader = header !== undefined ? header : (() => {
      if (typ.header === null)
        return null;
      const subheader =
        typeof typ.header === 'string' ? typ.header : `[${typ.header.join(', ')}]`;
      return `Multiple ${subheader} Values`;
    })();

    return new ItemType({
      name: `an array of ${BasicTypes.#dearticulate(typ.name)} values`,
      validate: (x, what) =>
        Array.isArray(x) &&
        x.every((y, i) => typ.checkType(y, `element ${i} of ${what}`)),
      csv: arr => wholeCSV(arr.map(x => typ.toCSV(x))),
      header: arrayHeader
    });
  }

  static frozenArray(typ, csv = undefined, header = undefined) {
    return BasicTypes.frozen(BasicTypes.array(typ, csv, header));
  }
  
  static #objectWithOrder(ordered) {
    const normalizeProperties = ordered ? _ => {} : props => props.sort();
    
    const properties = obj => {
      const props = Object.getOwnPropertyNames(obj);
      normalizeProperties(props);
      return props;
    };
    
    return (name, fields, csv = undefined, header = undefined) => {
      const wholeCSV = csv !== undefined ? csv : obj => JSON.stringify(obj);
      const objectHeader = header ?? null;
      
      const fieldNames = properties(fields);
      const fieldEntries = Object.entries(fields);
      
      return new ItemType({
        name,
        validate: (x, what) =>
          typeof x === 'object' &&
          x !== null &&
          arrayEqual(fieldNames, properties(x)) &&
          fieldEntries.every(([field, typ]) =>
            typ.checkType(x[field], `the "${field}" field in ${what}`)
          ),
        csv: obj => wholeCSV(
          Object.fromEntries(
            fieldEntries.map(([field, typ]) => [field, typ.toCSV(obj[field])])
          )
        ),
        header: objectHeader
      });
    };
  }

  static object = BasicTypes.#objectWithOrder(false);
  static orderedObject = BasicTypes.#objectWithOrder(true);
  
  static struct(name, fields, csv = undefined, header = undefined) {
    return BasicTypes.frozen(BasicTypes.orderedObject(name, fields, csv, header));
  }
  
  static objectRow(name, fields) {
    return BasicTypes.struct(
      name,
      fields,
      obj => Object.values(obj),
      Object.values(fields).map(typ => typ.header)
    );
  }
}
Object.freeze(BasicTypes);

class NYSEDTypes {
  constructor() {
    throw new Error('NYSEDTypes is a namespace of static methods');
  }
  
  static orNotGranting =
    BasicTypes.optionalWith(BasicTypes.literal('NOT-GRANTING'));

  static date = BasicTypes.regexp('a MMMM/YY date', monthYearRx, null);

  static fuzzyDate = BasicTypes.regexp(
    'a MMMM/YY date or PRE-YY date range',
    new RegExp(String.raw`${monthYearRx.source}|PRE-\d{2}`, 'u'),
    null
  );

  static conditionalDate = BasicTypes.struct(
    'conditionally-approved date',
    {
      cond: BasicTypes.regexp('a single capital letter', /[A-Z]/u, null),
      date: NYSEDTypes.date
    },
    ({cond, date}) => `${cond} ${date}`
  );

  static visitTypeAndDate = BasicTypes.struct(
    'visit type and date',
    {
      type: BasicTypes.regexp('a visit type (a single capital letter)', /[A-Z]/u, null),
      date: NYSEDTypes.date
    },
    ({type, date}) => `${type} ${date}`
  );

  // Using a static initializer block for shorter names
  static program;
  static {
    const {
      // Imported
      regexp, bool, optional, struct, objectRow,
      // Renamed for better defaults
      nonemptyString: string, frozenArray: array
    } = BasicTypes;

    const {
      // Imported
      orNotGranting, fuzzyDate, conditionalDate, date, visitTypeAndDate
    } = NYSEDTypes;

    NYSEDTypes.program = objectRow('NYSED program object', {
      programCode:            regexp('program code', /(?:M\/[AI] )?\d{5}/u),
      programCodePlaceholder: bool.withHeader('Program code is placeholder?'),
      programTitle:           string.withHeader('Program title'),
      hegisID:                orNotGranting(regexp('HEGIS ID', /\d{4}(\.\d{2})?/u)),
      award:                  string.withHeader('Degree awarded'),
      creditsOrClassHours:    optional(string).withHeader('Credits/Class hours'),
      firstRegistered:        fuzzyDate.withHeader('First registered'),
      lastRegistered:         fuzzyDate.withHeader('Last registered'),
      conditionalDate:        optional(conditionalDate).withHeader('Conditional date'),
      tbtDate:                optional(date).withHeader('TBT date'),
      progressReport:         optional(string).withHeader('Progress report'),
      visitTypeAndDate:       optional(visitTypeAndDate).withHeader('Visit type & date'),
      tap:                    orNotGranting(bool).withHeader('TAP'),
      apts:                   orNotGranting(bool).withHeader('APTS'),
      vta:                    orNotGranting(bool).withHeader('VTA'),
      uc:                     regexp('unit code', /\d{2}/u),
      formats:                array(string).withHeader('Formats'),
      certificatesOrLicenses: array(
        struct(
          'certificate/license',
          {
            professionalAccreditor: optional(string),
            title:                  string,
            type:                   optional(string),
            effectiveDate:          optional(fuzzyDate)
          },
         ({professionalAccreditor, title, type, effectiveDate}) => {
            let str = '';
            if (professionalAccreditor !== '')
              str += `Accredited by ${professionalAccreditor}: `;
            str += title;
            if (type !== '')
              str += `, type ${type}`;
            if (effectiveDate !== '')
              str += `, effective ${effectiveDate}`;
            return str;
          }),
        arr => arr.join('; ')
      ).withHeader('Certificates/Licenses')
    });
  }
}
Object.freeze(NYSEDTypes);

class NYSEDProgram {
  constructor(obj) {
    Object.assign(this, obj);
    Object.freeze(this);
    NYSEDTypes.program.checkType(this, 'the constructor argument');
  }

  static fromEntry(entry) {
    const error = (msg, value) =>
      new ParseError(`${msg} when parsing a NYSED program record`, value);
    
    const {pop, remainingKeys} = (outer => {
      const entry = new Map(outer);

      const pop = key => {
        const value = entry.get(key);
        if (!entry.delete(key))
          throw error(`No value for field "${key}"`);
        return value;
      };

      const remainingKeys = () => new Set(entry.keys());

      return {pop, remainingKeys};
    })(entry);

    const programCode                        = pop('PROG CODE');
    const programCodePlaceholder             = pop('PLACE HOLDER');
    const programTitle                       = pop('PROGRAM TITLE');
    const hegisID                            = pop('HEGIS');
    const award                              = pop('AWARD');
    const creditsOrClassHours                = pop('CR/CL HRS');
    const firstRegistered                    = pop('1ST REG');
    const lastRegistered                     = pop('LST REG');
    const conditionalDate                    = pop('COND DATE');
    const tbtDate                            = pop('TBT DATE');
    const progressReport                     = pop('PRGRS REPRT');
    const visitTypeAndDate                   = pop('VISIT TYPE/DATE');
    const tap                                = pop('TAP');
    const apts                               = pop('APTS');
    const vta                                = pop('VTA');
    const uc                                 = pop('UC');
    const formats                            = pop('FORMATS');
    const professionalAccreditors            = pop('PROF ACCR');
    const certificateOrLicenseTitles         = pop('CERTIFICATE/LICENSE: TITLE');
    const certificateOrLicenseTypes          = pop('CERTIFICATE/LICENSE: TYPE');
    const certificateOrLicenseEffectiveDates = pop('CERTIFICATE/LICENSE: EFF DATE');

    const extraKeys = remainingKeys();
    if (extraKeys.size > 0)
      throw error('Extra fields', [...extraKeys]);

    const lengths = new Set(
      [
        programCode,
        programTitle,
        hegisID,
        award,
        tap,
        apts,
        vta,
        formats,
        professionalAccreditors,
        certificateOrLicenseTitles,
        certificateOrLicenseTypes,
        certificateOrLicenseEffectiveDates
      ].map(arr => arr.length)
    );
    if (lengths.size > 1)
      throw error('Mismatched field lengths', [...lengths]);
    const length = lengths.values().next().value;

    const [coreSpan, ...continuationSpans] = (() => {
      const spans = [];
      let start = 0;
      for (let i = 1; i < length; ++i) {
        const blankCode  = Schema.isBlank(programCode[i]);
        const blankTitle = Schema.isBlank(programTitle[i]);
        if (blankCode !== blankTitle) {
          throw error(
            'A program code and program title were not both present or both absent',
            {code: blankCode, title: blankTitle});
        }

        if (!blankCode) {
          spans.push(Object.freeze({start, end: i}));
          start = i;
        }
      }
      spans.push(Object.freeze({start, end: length}));

      return Object.freeze(spans);
    })();

    const isMissing = x => x === null || (typeof x === 'string' && Schema.isBlank(x));
    const isPresent = x => !isMissing(x);

    const optionalWith = ifMissing =>
      x => typeof x === 'string' && x === '' ? ifMissing : x;
    const optional = optionalWith(null);
    const orNotGranting = optionalWith('NOT-GRANTING');

    const build = ({start, end}, fixProgramCode) => {
      const the = (field, arr) => {
        if (!arr.slice(start+1, end).every(isMissing)) {
          throw error(
            `The field "${field}" had too many present items`,
            arr.slice(start, end));
        }
        
        return arr[start];
      };

      const certificatesOrLicenses = [];
      for (let i = start; i < end; ++i) {
        const professionalAccreditor = optional(professionalAccreditors[i]);
        const title = certificateOrLicenseTitles[i];
        const type = optional(certificateOrLicenseTypes[i]);
        const effectiveDate = optional(certificateOrLicenseEffectiveDates[i]);

        if (isMissing(title)) {
          if (professionalAccreditor === null && type === null && effectiveDate === null)
            continue;
          else
            throw error('A certificate/license title was missing');
        }

        const {valid, problem, yesno} =
          title === 'ACADEMIC MAJOR'
            ? { valid: x => x === null, problem: 'had an additional', yesno: '' }
            : { valid: x => x !== null, problem: 'was missing its',   yesno: 'not ' };
        
        const validate = (what, thing) => {
          if (!valid(thing)) {
            throw error(
              `A certificate/license ${yesno}of type "ACADEMIC MAJOR" ${problem} ${what}`,
              thing
            );
          }
        };

        validate('type', type);
        validate('effective date', effectiveDate);

        certificatesOrLicenses.push(Object.freeze({
          professionalAccreditor,
          title,
          type,
          effectiveDate
        }));
      }
      Object.freeze(certificatesOrLicenses);

      const theHEGISID = the('hegisID', hegisID);

      const boolNotGranting = theHEGISID === 'NOT-GRANTING' ? orNotGranting : x => x;
      
      return new NYSEDProgram({
        programCode:            fixProgramCode(the('programCode', programCode)),
        programCodePlaceholder,
        programTitle:           the('programTitle', programTitle),
        hegisID:                theHEGISID,
        award:                  the('award', award),
        creditsOrClassHours,
        firstRegistered,
        lastRegistered,
        conditionalDate,
        tbtDate,
        progressReport,
        visitTypeAndDate,
        tap:                    boolNotGranting(the('tap', tap)),
        apts:                   boolNotGranting(the('apts', apts)),
        vta:                    boolNotGranting(the('vta', vta)),
        uc,
        formats:                Object.freeze(formats.slice(start, end).filter(isPresent)),
        certificatesOrLicenses
      });
    };

    const coreProgram = build(coreSpan, code => code);
    const continuationPrograms = continuationSpans.map(span =>
      build(span, code => {
        switch (code) {
          case ' M/A':
            return `M/A ${coreProgram.programCode}`;
          case 'M/I':
            return `M/I ${coreProgram.programCode}`;
          default:
            throw error('Unexpected continuation program code', code);
        }
      })
    );
    
    return Object.freeze([coreProgram, ...continuationPrograms]);
  }

  static fromEntries(entries) {
    return entries.flatMap((entry, i) => {
      try {
        return NYSEDProgram.fromEntry(entry);
      } catch (exn) {
        if (exn instanceof ParseError && exn.index === undefined)
          exn.index = i;
        throw exn;
      }
    });
  }

  toCSVRow() {
    return Object.freeze(NYSEDTypes.program.toCSV(this));
  }

  static toCSV(programs) {
    const csv = programs.map(program => NYSEDTypes.program.toCSV(program));
    csv.unshift(NYSEDTypes.program.header);
    return Object.freeze(csv);
  }
}
Object.freeze(NYSEDProgram);

function serializeCSV(csv, linebreak = '\r\n') {
  if (typeof csv === 'string') {
    return /[,"\r\n]/u.test(csv) ? '"' + csv.replaceAll('"', '""') + '"' : csv;
  } else if (Array.isArray(csv)) {
    const allStrings = arr => arr.every(x => typeof x === 'string');
    if (allStrings(csv)) {
      return csv.map(field => serializeCSV(field)).join(',');
    } else if (csv.every(item => Array.isArray(item) && allStrings(item))) {
      return csv.map(field => serializeCSV(field) + linebreak).join('');
    }
  }

  throw new TypeError(
    'serializeCSV expects ' +
      'a string, an array of strings, or an array of arrays of strings'
  );
}

const nysedColumns = (() => {
  const string = s => s;

  const regexps = (what, ...rxen) => s => {
    if (!rxen.some(rx => rx.test(s)))
      throw new ParseError(`${what} was malformed`, s);
    return s;
  };

  const numericID = regexps('Numeric ID', /^[0-9]+$/u);

  const hegisID = regexps('HEGIS ID', /^[0-9]+(\.[0-9]+)?$/u, /^$/u);

  const fuzzyDate = regexps('MMMM/YY date or PRE-YY date range',
                            /^PRE-[0-9]{2}$/u,
                            new RegExp(String.raw`^${monthYearRx.source}$`, 'u'));

  const optFuzzyDate = s => s === '' ? null : fuzzyDate(s);

  const bool = s => {
    switch (s) {
      case 'Y':
        return true;
      case 'N':
        return false;
      default:
        throw new ParseError('Boolean column was neither "Y" nor "N"', s);
    }
  };

  const optBool = s => s === '' ? null : bool(s);

  const taggedDate = (what, tag, wrap, verb = 'was') => {
    const rx = new RegExp(String.raw`^(${tag.source ?? tag}) (${monthYearRx.source})$`,
                          'u');
    return s => {
      const match = s.match(rx);
      if (!match)
        throw new ParseError(`${what} ${verb} malformed`, s);
      return Object.freeze(wrap(match[1], match[2]));
    };
  };
  
  const condDate = taggedDate(
    'Cond date',
    /[A-Z]/u,
    (cond, date) => ({cond, date})
  );

  const tbtDate = taggedDate(
    'Tbt date',
    'T',
    (_T, date) => date
  );

  const visitTypeDate = taggedDate(
    'Visit type and date',
    /[A-Z]/u,
    (type, date) => ({type, date}),
    'were'
  );

  const {Required, Optional, Some, Many} = Column.Count;
  
  return [
    new Column('PROG CODE',                     Many,     string),
    new Column('PROGRAM TITLE',                 Many,     string),
    new Column('HEGIS',                         Some,     hegisID),
    new Column('AWARD',                         Some,     string),
    new Column('CR/CL HRS',                     Optional, string),
    new Column(
      ['1ST REG', Required, fuzzyDate],
      ['LST REG', Required, fuzzyDate]
    ),
    new Column(
      ['COND DATE', Optional, condDate],
      ['TBT DATE',  Optional, tbtDate]
    ),
    new Column('PRGRS REPRT',                   Optional, string),
    new Column('VISIT TYPE/DATE',               Optional, visitTypeDate),
    new Column('TAP',                           Some,     optBool),
    new Column('APTS',                          Some,     optBool),
    new Column('VTA',                           Some,     optBool),
    new Column('UC',                            Required, numericID),
    new Column('FORMATS',                       Many,     string),
    new Column('PROF ACCR',                     Many,     string),
    new Column('CERTIFICATE/LICENSE: TITLE',    Many,     string),
    new Column('CERTIFICATE/LICENSE: TYPE',     Many,     string),
    new Column('CERTIFICATE/LICENSE: EFF DATE', Many,     optFuzzyDate)
  ];
})();

function noncolumnarMI(map, line) {
  const match =
    line.match(/^ +(M\/I) WITH ([0-9]+ (?:[^ ]| (?! ))+) {2,}(NOT-GRANTING) *$/u);
  if (!match)
    throw new ParseError("Can't parse noncolumnar line", line);

  const [_matchLine, mi, title, notGranting] = match;
  
  for (const [name, values] of map) {
    if (!Array.isArray(values)) continue;
    values.push((() => {
      switch (name) {
        case 'PROG CODE':
          return mi;
        case 'PROGRAM TITLE':
          return title;
        case 'HEGIS':
        case 'AWARD':
          return notGranting;
        case 'TAP':
        case 'APTS':
        case 'VTA':
        case 'FORMATS':
        case 'PROF ACCR':
        case 'CERTIFICATE/LICENSE: TITLE':
        case 'CERTIFICATE/LICENSE: TYPE':
        case 'CERTIFICATE/LICENSE: EFF DATE':
          return '';
        default:
          throw new ParseError(`Unexpected array field "${name}"`);
      }
    })());
  }
  
  return map;
}

function convertNYSEDDocument(string) {
  const lines = string.split(/\r?\n/u);
  const iterator = new LineIterator(lines);
  const headingSpans = readPreamble(iterator);
  const schema = new Schema(headingSpans, nysedColumns, noncolumnarMI);
  const entries = readEntries(iterator, schema);
  const programs = NYSEDProgram.fromEntries(entries);

  return programs;
}

function thisNYSEDDocumentText() {
  return document.body.firstChild.firstChild.wholeText;
}

function thisNYSEDDocumentCSVText() {
  return serializeCSV(NYSEDProgram.toCSV(convertNYSEDDocument(thisNYSEDDocumentText())));
}

function download(data, mime, file) {
  if (typeof mime !== 'string')
    throw new TypeError('download requires the MIME type to be a string');
  if (typeof file !== 'string')
    throw new TypeError('download requires the filename to be a string');

  const blob = new Blob(Array.isArray(data) ? data : [data], { type: mime });
  const url = URL.createObjectURL(blob, { type: mime });
  const a = document.createElement('a');
  a.href = url;
  a.download = file;
  a.click();
}

function downloadThisNYSEDDocumentAsCSV() {
  const basename = location.pathname.replace(/^(?:.*\/)?([^\/]*?)(?:\.[^.\/]+)?$/u, '$1');
  download(thisNYSEDDocumentCSVText(), 'text/csv', `${basename}.csv`);
}

function runWhenDOMContentLoaded(f) {
  if (document.readyState === 'loading')
    document.addEventListener('DOMContentLoaded', () => f());
  else
    f();
}

runWhenDOMContentLoaded(downloadThisNYSEDDocumentAsCSV);
